import React from 'react';
import './assets/scss/Carousel.scss';
import DisplayCarousel from './components/displayCarousel/DisplayCarousel';
import Button from './components/buttons/Button';

// PIXABAY API PARAMS
const BASE_URI = 'https://pixabay.com/api/';
const API_KEY = `9656065-a4094594c34f9ac14c7fc4c39`;
const DEFAULT_QUERY = "beautiful+landscape";
const IMAGE_TYPE = "photo";
const SAFE_SEARCH = true;
const PER_PAGE = 6;

const PIXABAY_URI = BASE_URI+'?key='+API_KEY+'&q='+DEFAULT_QUERY+'&image_type='+IMAGE_TYPE+'&per_page='+PER_PAGE+'&safesearch='+SAFE_SEARCH;
	
export default class Carousel extends React.Component {
    constructor(props) {
        super();
        this.state = {
            isLoaded: false,
            previewImageUrl: [],
            currentImage: null,
            indexImage: 0
        };
    }

    async componentDidMount() {
        this.fetchData();
    }

    fetchData = async () => {
        fetch(`${PIXABAY_URI}`,
        {
            method: 'GET',
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        })
        // We get a response and receive the data in JSON format...
        .then(response => response.json())
        // ...then we update the state of our application
        .then(
            data => {
                this.setState({
                    previewImageUrl: data.hits,
                    isLoaded: true,
					currentImage: data.hits[0],
                    indexImage: Math.floor(data.hits.length / 2),
                })
            }
        )
        // If we catch errors instead of a response, let's update the app
        .catch(error => this.setState({ error, isLoading: false }));
    };
	
    changeStateToMoveImage(newIndex, previewImageUrl = []) {
        this.setState(prevState => ({
            indexImage: newIndex,
            currentImage: previewImageUrl[newIndex]
        }));
    }

    setImageToCenter() {
        const { previewImageUrl } = this.state;
		// Return a index to show target over image in the middle
		return Math.floor(previewImageUrl.length/2);
        //return this.CarouselUtil.moveTargetToMiddle(previewImageUrl);
    }

    goToNextSlide() {
        const { indexImage, previewImageUrl } = this.state;
        const newIndex = indexImage + 1;
        const targetMiddleIndex = this.setImageToCenter();

        if (indexImage === previewImageUrl.length - 1) {
            return this.changeStateToMoveImage(targetMiddleIndex, previewImageUrl);
        }
        this.changeStateToMoveImage(newIndex, previewImageUrl);
    }

    goToPrevSlide() {
        const { indexImage } = this.state;
        const newIndex = indexImage - 1;
        const targetMiddleIndex = this.setImageToCenter();
		
        // Receives a indexImage to check if the pagination has arrived at beginning or position zero
        if (indexImage === 0) {
            return this.changeStateToMoveImage(targetMiddleIndex);
        }
        this.changeStateToMoveImage(newIndex);
    }

    move(index, images = []) {
        if (images === 0 || images.length === 0) { return 0 };
            return Math.floor(index * (100 / images.length));
    }
	
    moveImage(index, previewImageUrl) {
        const newPosition = this.move(index, previewImageUrl);
        // Receives a newPosition to move image
        return `translateX(-${newPosition}%)`;
    }

    render () {
        const { isLoaded, previewImageUrl, indexImage } = this.state;
        return (
            <div className="main-container">
                <div className="container">
                    <div className={`carousel-container active-${indexImage}`}>
                        <div className="carousel-wrapper" style={{'transform': this.moveImage(indexImage, previewImageUrl)}}>
                            <DisplayCarousel isLoaded={isLoaded} key={previewImageUrl.id} imageUrl={previewImageUrl} />
                        </div>
                    </div>
                </div>
                <div className='controls'>
                    <Button title="Prev" type="prev" onclick={() => this.goToPrevSlide() } />
                    <Button title="Next" type="next" onclick={() => this.goToNextSlide(indexImage)} />
                </div>
            </div>
        )
    }
}
