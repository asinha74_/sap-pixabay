import React, { Component } from 'react';

class DisplayCarousel extends Component {

    render() {
    const { isLoaded, imageUrl } = this.props;
    const LoadingIndicator = () => <div className="loading-indicator">Loading...</div>;
	
    return isLoaded
        ? imageUrl.map(img => (
            <div key={img.id} className="details">
                <div className="container-img">
                    <img src={img.previewURL} alt={img.previewURL} />
					<p>{img.tags}</p>
                </div>
            </div>
        ))
        : <LoadingIndicator />;
    }
}

export default DisplayCarousel;
